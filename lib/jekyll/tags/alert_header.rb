module Jekyll
  module Tags
    class AlertHeaderTag < Liquid::Block
      def initialize(tag_name, block_options, liquid_options)
        super
      end

      def render(context)
        context.stack do
          @content = super
        end
        output = "<h3>#{@content}</h3>"
      end
    end
  end
end

Liquid::Template.register_tag('alert_header', Jekyll::Tags::AlertHeaderTag)
