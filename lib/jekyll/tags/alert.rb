module Jekyll
  module Tags
    class AlertTag < Liquid::Block
      def initialize(tag_name, block_options, liquid_options)
        super
        @class = block_options.strip
      end

      def render(context)
        context.stack do
          context["class"] = @class
          @content = super
        end
        output = <<~EOS
            <div class="alert #{@class}">
              #{@content}
            </div>
        EOS
      end
    end
  end
end

Liquid::Template.register_tag('alert', Jekyll::Tags::AlertTag)
