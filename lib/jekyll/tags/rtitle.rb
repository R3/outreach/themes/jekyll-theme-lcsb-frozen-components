module Jekyll
    module Tags
      class RtitleTag < Liquid::Block

        def split_params(params)
            params.split("|").map(&:strip)
        end

        def initialize(tag_name, block_options, liquid_options)
            @markup = block_options
            super

            args = split_params(block_options)

            # Optional - setting your own ID
            if args.length > 1
                @block_id_html = " id=\"#{args[1]}\""
            else
                @block_id_html = ""
            end
        end

        # similar solution proposed here: https://stackoverflow.com/a/45393697
        def get_value(context, expr)
            args = split_params(expr)

            # title in quotes, either double or single
            if (expr[0]=='"' and expr[-1]=='"') or (expr[0]=="'" and expr[-1]=="'")
                return expr[1..-2]

            # expression contains a dot and is not longer than 20 characters
            elsif expr.start_with? "{{" and expr.end_with? "}}"
                expr = expr[2..-3].strip
                vars = expr.split('.')
                result = context
                vars.each do |variable|
                    result = result[variable] if result
                end
                return result

            # title is given as a sequence of words
            else
                return args[0]
            end
        end

        def render(context)
            site = context.registers[:site]
            converter = site.find_converter_instance(::Jekyll::Converters::Markdown)
            content = converter.convert(super)
            title = get_value(context, @markup.strip)
            output = <<~EOS
<div class="rtitle"#{@block_id_html}>
<h2>#{title}</h2>
<hr>
    #{content}
</div>
    EOS

            output
        end
      end
    end
  end

Liquid::Template.register_tag('rtitle', Jekyll::Tags::RtitleTag)
