module Jekyll
    module Tags
      class RblockTag < Liquid::Block
       
        def split_params(params)
            params.split("|").map(&:strip)
        end

        def initialize(tag_name, block_options, liquid_options)
            super
            args = split_params(block_options)
            @title = args[0]

            # Optional - using your own icons
            if args.length > 1
                @icon = args[1]
            else
                # Default icon
                @icon = "fas fa-code"
            end

            # Optional - setting your own ID
            if args.length > 2
                @block_id_html = " id=\"#{args[2]}\""
            else
                @block_id_html = ""
            end
        end
  
        def render(context)
            rgridblockID = context["rgridblockID"]
           
            #content = super
            site = context.registers[:site]
            converter = site.find_converter_instance(::Jekyll::Converters::Markdown)
            content = converter.convert(super)
                
            #Because we’re not changing the context, there’s no need to push the context stack as with the rgridblock. 
            # generate collapsible card HTML
            output = <<~EOS
<div class="rblock"#{@block_id_html}>
    <h3>
    <i class="#{@icon}"></i>
    #{@title}
    </h3>
    <hr>
    <p>
    #{content}
    </p>
</div>
    EOS
             output
        end
      end
    end
  end
  
 


Liquid::Template.register_tag('rblock', Jekyll::Tags::RblockTag)
