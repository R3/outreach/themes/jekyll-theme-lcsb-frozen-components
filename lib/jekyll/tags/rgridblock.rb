module Jekyll
    module Tags
        class RgridblockTag < Liquid::Block
            def initialize(tag_name, block_options, liquid_options)
                super
                @rgridblockID = "rgridblock-#{block_options.strip}"
            end



            def render(context)
                #Pushes a new local scope on the stack, pops it at the end of the block
                #Each new rgridblock  creates a new scope, and contexts have a custom implementation of [] to search up the stack for a matching value.
                context.stack do
                    context["rgridblockID"] = @rgridblockID
                    @content = super
                end
                # The last bit we add is saving the resulting output of our block contents to an instance variable so we can access it outside of the stack scope.
                output= %(<div class="rgridblock" id="#{@rgridblockID}">#{@content}</div>)
                output
              
            end
        end
    end
end

Liquid::Template.register_tag('rgridblock', Jekyll::Tags::RgridblockTag)
