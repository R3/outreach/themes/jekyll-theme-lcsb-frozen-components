# Frozen components

## What is that?
A collection of Jekyll plugins (tags and filters) that allow the user to include components from _frozen-pages_ layout seamlessly.

## How to use it in your repository?
There are two changes to be made in your Jekyll project in order to use this:
* in `Gemfile`, inside `group :jekyll_plugins do` add the following:

```
 gem 'jekyll-theme-lcsb-frozen-components', 
     '~> 0.0.2', 
     :git => "https://git-r3lab.uni.lu/core-services/jekyll-theme-lcsb-frozen-components.git", 
     :branch => "master"
```

Example:

```
group :jekyll_plugins do
 gem "jekyll-paginate-v2", 
     "~> 2",
     :git => "https://github.com/sverrirs/jekyll-paginate-v2.git"

 gem "jekyll-feed", 
     "~> 0.6"

 gem 'jekyll-theme-lcsb-frozen-components', 
     '~> 0.0.2', 
     :git => "https://git-r3lab.uni.lu/core-services/jekyll-theme-lcsb-frozen-components.git", 
     :branch => "master"
end
```

* in `_config.yml`, inside `plugins:`, add the following:

```
  - jekyll-theme-lcsb-frozen-components
```

Example:

```
plugins:
  - jekyll-feed
  - jekyll-paginate-v2
  - jekyll-theme-lcsb-frozen-components
```

## How to actually use it?

### RTitle
It is the element to create title box - usually it would contain the paper's title, authors and some basic information about the project.
```
{% rtitle Here goes the title | html-id-of-the-element %}
Here goes the content of the box.
{% endrtitle %} 
```

### RGridBlock
This is a container, which keeps the smaller boxes (_rblocks_).

```
{% rgridblock a-unique-html-id %}
    // all of the rblocks must be here!
{% endrgridblock %}
```

### RBlock
This is the small box element. It supports font-awesome's icons (take a look on [https://fontawesome.com/icons?d=gallery](https://fontawesome.com/icons?d=gallery)).
```
{% rgridblock a-unique-html-id %}       // don't forget about it!
   {%  rblock Title of a box | icon CSS classes, like: fas fa-globe | html-id-of-the-element %}
       Here goes the content of the box.
   {% endrblock %}
{% endrgridblock %}
```

## Developer's notes

1. **Note, that this repository does not contain CSS files!** They stay within the theme.
2. The code is contained in `lib` directory
2. When you add a new file containing source code, don't forget to register it in `lib/jekyll-theme-lcsb-frozen-components.rb` with `require`.

## License
**Apache2**, see LICENSE
